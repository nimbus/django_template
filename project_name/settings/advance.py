# -*- coding:utf-8 -*-
"""
Django settings for {{ project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/{{ docs_version }}/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from .base import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/{{ docs_version }}/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '{{ secret_key }}'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*', ]

ADMINS = [
    ("admin", "william_ren@sina.cn")
]

MANAGERS = ADMINS

# Application definition

INSTALLED_APPS = [
    'admin_interface',
    'flat_responsive',
    # 'flat', # only if django version < 1.9
    'colorfield',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'markdown',
    'corsheaders',
    'rest_framework',
    'url_filter',

    'compressor',
    'constance',
    # 'constance.backends.database',
    'post_office',
    'import_export',
    'jsonfield',
    'storages',

    'raven.contrib.django.raven_compat',

    # apps
    'api.apps.AppConfig',
    'app.apps.AppConfig',
]

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]

ROOT_URLCONF = '{{ project_name }}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = '{{ project_name }}.wsgi.application'


# Database
# https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/{{ docs_version }}/topics/i18n/

LANGUAGE_CODE = 'zh-hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DATETIME_FORMAT = 'Y-m-d H:i:s'

DATE_FORMAT = 'Y-m-d'

_ = lambda s: s

LANGUAGES = [
    ('en', _('English')),
    ('zh', _(u'简体中文')),
    ('zh-hans', _(u'简体中文')),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/{{ docs_version }}/howto/static-files/

# ASSETS
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (os.path.join(SETTING_DIR, 'static'),)
# MEDIA
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# 翻译文件所在目录，需要手工创建
LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'),)

###########################################
# SETTINGS
###########################################

try:
    from .settings_log import *
except ImportError as e:
    pass

try:
    from .settings_email import *
except ImportError as e:
    pass

try:
    from .settings_s3 import *
except ImportError as e:
    pass

try:
    from .settings_rest import *
except ImportError as e:
    pass

try:
    from .settings_ie import *
except ImportError as e:
    pass

try:
    from .settings_cache import *
except ImportError as e:
    pass

try:
    from .settings_constance import *
except ImportError as e:
    pass

try:
    from .settings_celery import *
except ImportError as e:
    pass

try:
    from .settings_extra import *
except ImportError as e:
    pass

try:
    from .settings_raven import *
except ImportError as e:
    pass

###########################################
# ENVIRONMENTS
###########################################
try:
    if is_prod():
        from .settings_prod import *
    elif is_test():
        from .settings_test import *
    elif is_dev():
        from .settings_dev import *
    elif is_ab():
        from .settings_ab import *
except ImportError:
    pass
