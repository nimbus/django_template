# -*- coding: utf-8 -*-
import os
import re
from .base import BASE_DIR

DEBUG = False

ALLOWED_HOSTS = ['*', ]

ADMINS = [
    ('william', 'wenlong.ren@yidatec.com'),
]

MANAGERS = ADMINS

EMAIL_SUBJECT_PREFIX = "[{{ project_name|upper }}_TEST]"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        # 'ENGINE': 'mysql.connector.django',
        'NAME': '{{ project_name|lower }}',
        'USER': '{{ project_name|lower }}',
        'PASSWORD': '{{ project_name|lower }}',
        'HOST': '{{ project_name|lower }}.com.cn',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': "SET default_storage_engine=INNODB, sql_mode='STRICT_TRANS_TABLES'",
        },
    },
}

INTERNAL_IPS = ['127.0.0.1', ]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

CSRF_TRUSTED_ORIGINS = [
    "{{ project_name|lower }}.xxx.com",
]

SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_AGE = 5*60
SESSION_EXPIRE_AT_BROWSER_CLOSE = False

SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = True
SECURE_SSL_HOST = "{{ project_name|lower }}.xxx.com"

SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True

CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True

X_FRAME_OPTIONS = 'DENY'

# CSP_DEFAULT_SRC = ("'self'", "'unsafe-inline'", )
CSP_DEFAULT_SRC = ("'self'", )
CSP_SCRIPT_SRC = ("'self'", "'unsafe-inline'", "data:", )
CSP_IMG_SRC = ("'self'", "data:", )
CSP_STYLE_SRC = ("'self'", "'unsafe-inline'", "data:", )
CSP_FONT_SRC = ("'self'", "'unsafe-inline'", "data:", )
