# -*- coding: utf-8 -*-
from .base import *

if is_prod():
    _DB_DEBUG = False
    _CONSOLE_FILTER = ['require_debug_true']
elif is_test():
    _DB_DEBUG = False
    _CONSOLE_FILTER = ['require_debug_false']
elif is_dev():
    _DB_DEBUG = False
    _CONSOLE_FILTER = ['require_debug_true']
elif is_ab():
    _DB_DEBUG = False
    _CONSOLE_FILTER = ['require_debug_true']
else:
    _DB_DEBUG = True
    _CONSOLE_FILTER = ['require_debug_true']

# Log
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
        'django': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S",
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console_filter': {
            'level': 'DEBUG',
            'filters': _CONSOLE_FILTER,
            'class': 'logging.StreamHandler',
            'formatter': 'django'
        },
        "console": {
            "level": "DEBUG",
            'class': 'logging.StreamHandler',
            'formatter': 'django'
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose',
            'filters': ['require_debug_true'],
        },
        # 'file': {
        #     'level': 'DEBUG',
        #     'class': 'logging.FileHandler',
        #     'filename': LOG_FILE,
        #     'formatter': 'verbose'
        # },
        'null': {
            "class": 'logging.NullHandler',
        },
        'sentry': {
            'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console_filter'],
        },
        'django.db.backends': {
            'handlers': ['console_filter'],
            'level': 'DEBUG' if _DB_DEBUG else 'INFO',
        },
        'django.request': {
            'handlers': ['mail_admins', 'console_filter'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['mail_admins', ],
            'level': 'ERROR',
            'propagate': False,
        },
        'py.warnings': {
            'handlers': ['null'],
        },
        "post_office": {
            "handlers": ["console_filter", ],
            "level": 'DEBUG' if _DB_DEBUG else 'INFO',
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'root': {
            'handlers': ['console_filter', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    },
}