#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
import os
import re
import base64
import json
import datetime
import traceback
from collections import defaultdict

# 通常你不应该从django引入任何代码, 但ImproperlyConfigured是个例外
from django.core.exceptions import ImproperlyConfigured

__all__ = [
    'PWD',
    'SETTING_DIR',
    'BASE_DIR',
    'APP',
    'LOG_FILE',
    'is_prod',
    'is_test',
    'is_dev',
    'is_ab',
    'get_version',
    'get_full_version',
]

PWD = os.path.dirname(os.path.abspath(__file__))
SETTING_DIR = os.path.abspath(os.path.dirname(PWD))
BASE_DIR = os.path.dirname(SETTING_DIR)
APP = os.path.basename(SETTING_DIR)
LOG_FILE = os.path.abspath(os.path.join(BASE_DIR, "%s.log" % (APP,)))

# DEFAULT_ENVIRONMENT = "prod"
# DEFAULT_ENVIRONMENT = "test"
DEFAULT_ENVIRONMENT = "dev"
DEFAULT_VERSION = "1.0.0"
os.environ.setdefault("PROJECT_ENVIRONMENT", DEFAULT_ENVIRONMENT)
os.environ.setdefault("PROJECT_VERSION", DEFAULT_VERSION)

sys.path.insert(0, os.path.join(BASE_DIR, "libs"))
sys.path.append("/usr/lib/python2.7/dist-packages")
sys.path.append("/usr/local/lib/python2.7/site-packages")
sys.path.append("/Library/Python/2.7/site-packages")


def get_environment(default=DEFAULT_ENVIRONMENT):
    try:
        env = get_env_variable("PROJECT_ENVIRONMENT", default)
        print u"PROJECT_ENVIRONMENT:{} default:{}".format(env, default)
        return str(env)
    except ImproperlyConfigured as e:
        traceback.print_exc()
        return default


def get_env_variable(var_name, default=None):
    try:
        return os.environ.get(var_name, default)
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)


ENVIRONMENT = get_environment()


def is_prod():
    return ENVIRONMENT in ["p", "pro", "prod", "production", ]


def is_test():
    return ENVIRONMENT in ["t", "test", ]


def is_dev():
    return ENVIRONMENT in ["d", "dev", ]


def is_ab():
    return ENVIRONMENT in ["a", "ab", ]


def get_version(default=DEFAULT_VERSION):
    version = get_env_variable("PROJECT_VERSION", default)
    return version


def get_full_version(default=DEFAULT_VERSION):
    v = get_version(default)
    return u"{version}_{env}".format(version=v, env=ENVIRONMENT)


