# -*- coding: utf-8 -*-
from __future__ import absolute_import
from .base import *

APPEND_SLASH = True

PAGE_QUERY_PARAM = 'page'
PAGE_SIZE_QUERY_PARAM = 'page_size'
MAX_PAGE_SIZE = 1000

REST_FRAMEWORK = {
    'PAGE_SIZE': 200,

    'SEARCH_PARAM': 's',
    'ORDERING_PARAM': 'o',

    'DEFAULT_VERSION': 'v3',
    'ALLOWED_VERSIONS': ['v3', ],
    'DEFAULT_VERSIONING_CLASS': 'django_nimbus_api.versioning.HeaderVersioning',
    'DEFAULT_PAGINATION_CLASS': 'django_nimbus_api.pagination.APIPageNumberPagination',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.

    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],


    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAdminUser',
        'rest_framework.permissions.IsAuthenticated',
    ],

    'DEFAULT_FILTER_BACKENDS': [
        'rest_framework.filters.SearchFilter',
        'rest_framework.filters.OrderingFilter',
        'url_filter.integrations.drf.DjangoFilterBackend',
    ],

    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.AdminRenderer',
        'rest_framework.renderers.TemplateHTMLRenderer',
    ],

    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ],

    'DEFAULT_THROTTLE_CLASSES': [
        'django_nimbus_api.throttling.ScopedRateThrottle',
        'django_nimbus_api.throttling.UserRateThrottle',
    ],

    'DEFAULT_THROTTLE_RATES': {
        'login': None,
        'user': None,
    },

    'EXCEPTION_HANDLER': 'django_nimbus_api.views.exception_handler',

    'DATETIME_FORMAT': "%Y-%m-%d %H:%M:%S",
    'DATE_FORMAT': "%Y-%m-%d",
    'TIME_FORMAT': "%H:%M:%S",
}

if is_ab():
    REST_FRAMEWORK["ALLOWED_VERSIONS"] = ['v0', 'v1', 'v2', 'v3', ]
elif is_prod():
    REST_FRAMEWORK["ALLOWED_VERSIONS"] = ['v3', ]
    REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] = [
        'rest_framework.renderers.JSONRenderer',
    ]
elif is_dev():
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = [
    ]
    REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = [
        'rest_framework.permissions.AllowAny',
    ]

