# -*- coding: utf-8 -*-
import os
import re
from .base import BASE_DIR

DEBUG = True

ALLOWED_HOSTS = ['*', ]

ADMINS = [
    ('william', 'wenlong.ren@yidatec.com'),
]

MANAGERS = ADMINS

EMAIL_SUBJECT_PREFIX = "[{{ project_name|upper }}_DEV]"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
}

INTERNAL_IPS = ['127.0.0.1', ]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

CSRF_TRUSTED_ORIGINS = [
    "{{ project_name|lower }}.xxx.com",
]


SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_AGE = 5*60
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

SECURE_HSTS_SECONDS = 0
SECURE_HSTS_INCLUDE_SUBDOMAINS = False
SECURE_CONTENT_TYPE_NOSNIFF = False
SECURE_BROWSER_XSS_FILTER = False
SECURE_SSL_REDIRECT = False

SESSION_COOKIE_SECURE = False
SESSION_COOKIE_HTTPONLY = True

CSRF_COOKIE_SECURE = False
CSRF_COOKIE_HTTPONLY = False

X_FRAME_OPTIONS = 'SAMEORIGIN'

CSP_DEFAULT_SRC = ("'self'", )
CSP_SCRIPT_SRC = ("'self'", "'unsafe-inline'", "data:", )
CSP_IMG_SRC = ("'self'", "data:", )
CSP_STYLE_SRC = ("'self'", "'unsafe-inline'", "data:", )
CSP_FONT_SRC = ("'self'", "'unsafe-inline'", "data:", )
