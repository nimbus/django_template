from __future__ import absolute_import, unicode_literals
import os
from distutils.version import StrictVersion
from django.conf import settings
from celery import Celery
from celery import VERSION

PWD = os.path.dirname(os.path.abspath(__file__))
APP = os.path.basename(PWD)

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', '%s.settings' % APP)

app = Celery(APP)

# Using a string here means the worker will not have to
# pickle the object when using Windows.
if VERSION.major >= 4:
    app.config_from_object('django.conf:settings', namespace='CELERY')
    app.autodiscover_tasks()
else:
    app.config_from_object('django.conf:settings')
    app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True, name=u"celery debug")
def debug_task(self):
    print 'Request: {0!r}'.format(self.request)
