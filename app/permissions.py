# -*- coding: utf-8 -*-
import re
import uuid
import logging
import collections
from django.conf import settings
from django.db import models
from django.dispatch import receiver
from django.db.models import F, Q
from django.db.models import Count, Avg, Sum, Aggregate, Max, Min
from django.db.models.signals import post_save, post_delete
from django.db.models.signals import class_prepared
from django.forms.models import model_to_dict, fields_for_model, ALL_FIELDS
from django.urls import reverse
from django.shortcuts import resolve_url
from django.core import validators
from django.utils import timezone
from django.utils import dateformat
from django.utils.timezone import make_aware, make_naive, is_aware, is_naive, now, utc
from django.utils.encoding import smart_str, smart_unicode
from django.utils.encoding import force_text, python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from rest_framework import filters, permissions, authentication
from .models import *

logger = logging.getLogger(__name__)


class IsAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated


