# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import json
from django.contrib import admin
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Avg, Count, F, Q
from django.db.models.fields import BLANK_CHOICE_DASH
from django.contrib.admin.models import LogEntry, DELETION
from django.contrib.sessions.models import Session
from django.utils.html import escape
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext, ugettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin

from .models import *
