"""
URLconf for registration and activation, using django-registration's
one-step backend.

If the default behavior of these views is acceptable to you, simply
use a line like this in your root URLconf to set up the default URLs
for registration::

    (r'^accounts/', include('registration.backends.simple.urls')),

This will also automatically set up the views in
``django.contrib.auth`` at sensible default locations.

If you'd like to customize registration behavior, feel free to set up
your own URL patterns for these views instead.

"""

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from rest_framework import routers

from app.viewsets import ExampleAPIView


router = routers.DefaultRouter(trailing_slash=True)
router.register(r'ex', ExampleAPIView)


urlpatterns = [
    url(r'^', include(router.urls)),
]

