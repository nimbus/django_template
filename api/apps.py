# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig as DJAppConfig
from django.utils.translation import ugettext_lazy as _


class AppConfig(DJAppConfig):
    name = 'api'
    verbose_name = _('10.API')

