# -*- coding: utf-8 -*-
import os
import re
import django
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.db.models.signals import class_prepared
from django.db.models import Avg, Count, F, Q
from django.contrib.auth.models import User, Group, Permission, ContentType
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_text, python_2_unicode_compatible
from django.utils import timezone
from django.contrib.auth import get_user_model
